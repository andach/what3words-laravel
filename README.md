# what3words-laravel
This is a Laravel package to automate the "What 3 Words" addressing system. 

## Installation
```
composer require andach/what3words-laravel
```

## Usage
### Getting a single address
```php
use Andach\LaravelWhat3Words\What3Words;

$address = new What3Words("filled.count.soap");
// or
$address = new What3Words("filled", "count", "soap");

var_dump($address->getCoordinates());   // [51.520847, -0.195521]
var_dump($address->getCountry());       // "GB"
var_dump($address->getLatitude());      // 51.520847
var_dump($address->getLongtitude());    // -0.195521
var_dump($address->getMap());           // "https://w3w.co/filled.count.soap"
var_dump($address->getNearestPlace());  // "Bayswater, London"
var_dump($address->getWords());         // "filled.count.soap"
```

### Comparing two addresses
```php
$otherAddress = new What3Words("filled.count.soap");


var_dump($address->distanceFrom(What3Words $otherLocation, String $transformationMethod)); // float distanceInMeters
// $transformationMethod = one of ['crowflies', 'car', 'publictransport', 'throughearth', 'walking'];
```
