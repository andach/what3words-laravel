<?php

namespace Andach\LaravelWhat3Words;

class What3Words {
	private $wordOne;
	private $wordTwo;
	private $wordThree;

	function __construct($wordOne = "", $wordTwo = "", $wordThree = "") 
	{
		$this->apiKey = config('what3words.apikey');

		if (!$this->apiKey)
		{
			// throw exception
		}

		if (!$this->setThreeWords($wordOne, $wordTwo, $wordThree))
		{
			// throw exception
		}
	}

	public function distanceFrom(What3Words $otherWords, $transportationMethod = 'crowflies')
	{
		switch ($transportationMethod)
		{
			case 'crowflies' :
				return 1;
				break;
		}

		return 0;
	}

	public function getCoordinates()
	{

	}

	public function getCountry()
	{

	}

	public function getNearestPlace()
	{

	}

	public function getWords()
	{

	}

	public function prepareWord($word)
	{
		return preg_replace('/[^a-z\.]/', '', strtolower($word));
	}

	public function setFromApi()
	{
		$url = 'https://api.what3words.com/v3/convert-to-coordinates?'.$this->wordsWithDots.'&key='.$this->apiKey;
		$result = file_get_contents($url);

		$this->json = json_decode($result, true);
	}

	public function setThreeWords($wordOne = "", $wordTwo = "", $wordThree = "") 
	{
		$wordOne   = $this->prepareWord($wordOne);
		$wordTwo   = $this->prepareWord($wordTwo);
		$wordThree = $this->prepareWord($wordThree);

		$this->wordOne   = $wordOne;
		$this->wordTwo   = $wordTwo;
		$this->wordThree = $wordThree;

		if (!$this->wordTwo || !$this->wordThree)
		{
			$array = explode('.', $wordOne);

			$this->wordOne   = $array[0];
			$this->wordTwo   = $array[1];
			$this->wordThree = $array[2];
		}

		if ($this->wordOne && $this->wordTwo && $this->wordThree)
		{
			$this->setFromApi();
		}

		$this->wordsWithDots = $this->wordOne.'.'.$this->wordTwo.'.'.$this->wordThree;
	}
}

