<?php

namespace Andach\LaravelWhat3Words\Facades;

use Illuminate\Support\Facades\Facade;

class What3Words extends Facade 
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'what3words'; }
}