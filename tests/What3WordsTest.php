<?php

namespace Andach\LaravelWhat3Words\Tests;

require_once __DIR__ . '/../vendor/autoload.php';

use Andach\LaravelWhat3Words\What3Words;

class What3WordsTest extends \PHPUnit_Framework_TestCase
{
    public function basicTest()
    {
        $address = new What3Words("filled.count.soap");

        $this->assertEquals($address->getCoordinates(), [51.520847, -0.195521]);
        $this->assertEquals($address->getCountry(), 'GB');
        $this->assertEquals($address->getLatitude(), 51.520847);
        $this->assertEquals($address->getLongtitude(), -0.195521);
        $this->assertEquals($address->getMap(), 'https://w3w.co/filled.count.soap');
        $this->assertEquals($address->getNearestPlace(), 'Bayswater, London');
        $this->assertEquals($address->getWords(), 'filled.count.soap');
    }

    public function failureTest()
    {
        $address = new What3Words("does.not.exist");
    }

    public function setUp()
    {
        
    }

    public function tearDown()
    {
        
    }
}
